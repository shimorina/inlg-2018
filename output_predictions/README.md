Each folder contains:

1. `*_predictions.txt`

	Output of a model based on test files in `input_data`.

2. `relex_*` (WebNLG) or `predictions_*` (E2E)
	
	Relexicalised predictions where delexicalised entities were recovered from initial input.
	Since test files include repeated (data, text) pairs, those were deleted.
		

3. `refs_*`
	
	References for scoring.	
	
4. `metrics_*`

	Output of an automatic evaluation script.
