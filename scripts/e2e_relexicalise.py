from e2e_data_reading import E2EDataset
from e2e_generate_constraints import generate_constraints


def relexicalise(path_to_model, preds_file, constraints=True):
    e2e = E2EDataset()
    if not constraints:
        # read from the original csv test file
        path = '/home/anastasia/Loria/thesis/e2e-dataset/'
        file_test = 'testset_w_refs.csv'
        e2e.fill_dataset(path + file_test)
        # tuples of replacements for each line
        replacements = e2e.write_data('test-delex', delex=True, writefiles=False)
    else:
        e2e = generate_constraints(write_to_file=False)  # returns test set
        # tuples of replacements for each line
        replacements = e2e.write_data('test-constraints-delex', delex=True, writefiles=False)

    # read predictions
    with open(path_to_model + preds_file, 'r') as f:
        predictions = f.readlines()
    preds_relex = []
    for pred, replacement in zip(predictions, replacements):
        pred_relex = pred
        for item in replacement:
            # [(Restaurant, NAME), (Restaurant, NEAR)]
            entity, attr = item
            if attr in pred:
                pred_relex = pred_relex.replace(attr, entity)
        preds_relex.append(pred_relex)

    # write to file
    with open(path_to_model + 'relex_predictions.txt', 'w+') as f:
        f.write(''.join(preds_relex))


def postprocess(pred_file, ref_file, mrs_file, path_data, option=''):
    """
    Write references and predictions for the input to e2e metrics.
    :param pred_file:
    :param ref_file:
    :param mrs_file:
    :param path_data:
    :param option:
    :return:
    """
    with open(pred_file, 'r') as f:
        preds = f.readlines()
    with open(ref_file, 'r') as f:
        refs = f.readlines()
    with open(mrs_file, 'r') as f:
        mrs = f.readlines()
    # {mr: [pred, ref1, ref2, ref3]}
    mrs_refs = {}
    for mr, ref, pred in zip(mrs, refs, preds):
        if mr not in mrs_refs.keys():
            mrs_refs[mr] = [pred.strip(), ref.strip()]
        else:
            mrs_refs[mr].append(ref.strip())
    print('Test instances: ', len(mrs_refs))

    count = 0
    with open(path_data + 'refs_' + option + '.txt', 'w+') as f_ref:
        with open(path_data + 'predictions_' + option + '.txt', 'w+') as f_pred:
            for refs in sorted(mrs_refs.values()):
                count += 1
                # except the first one which is the prediction
                # f_ref.write(str(count) + '\n' + '\n'.join(refs[1:]) + '\n\n')  # write counts
                f_ref.write('\n'.join(refs[1:]) + '\n\n')
                if len(refs[1:]) == 0:
                    print(refs)
                f_pred.write(refs[0] + '\n')

    print(count)


# path to output_predictions
path_models = '/home/anastasia/Loria/opennmtpy_models/'


def gen_refs_files():
    # No constraints
    path_data = 'e2e_files/original_split_opennmtpy/'	# path to input_data
    ref_file = path_data + 'test-notdelex-e2e.lex'
    mrs_file = path_data + 'test-notdelex-e2e.mrs'

    # nodelex
    pred_file = path_models + 'e2e_nodelex/e2e-nodelex_predictions.txt'
    postprocess(pred_file, ref_file, mrs_file, path_data, option='nodelex')

    # nodelex, copy and coverage
    pred_file = path_models + 'e2e_nodelex/copy-coverage/e2e-nodelex-copy-coverage_predictions.txt'
    postprocess(pred_file, ref_file, mrs_file, path_data, option='nodelex-copy-coverage')

    # delex, relexicalise
    path_to_model = path_models + 'e2e_delex/'
    preds_file_initial = 'e2e-delex_predictions.txt'
    relexicalise(path_to_model, preds_file_initial, constraints=False)

    pred_file = path_models + 'e2e_delex/relex_predictions.txt'
    postprocess(pred_file, ref_file, mrs_file, path_data, option='delex')

    # delex, relexicalise, copy and coverage
    path_to_model = path_models + 'e2e_delex/copy-coverage/'
    preds_file_initial = 'e2e-delex-copy-coverage_predictions.txt'
    relexicalise(path_to_model, preds_file_initial, constraints=False)

    pred_file = path_models + 'e2e_delex/copy-coverage/relex_predictions.txt'
    postprocess(pred_file, ref_file, mrs_file, path_data, option='delex-copy-coverage')


# gen_refs_files()


def gen_constraints_refs_files():
    # With constraints
    path_data = 'e2e_files/constraints_opennmtpy/'		# path to input_data
    ref_file = path_data + 'test-constraints-notdelex-e2e.lex'
    mrs_file = path_data + 'test-constraints-notdelex-e2e.mrs'

    # nodelex
    pred_file = path_models + 'e2e_constraints_nodelex/e2e-constraints-nodelex_predictions.txt'
    postprocess(pred_file, ref_file, mrs_file, path_data, option='constraints-nodelex')

    # nodelex, copy and coverage
    pred_file = path_models + 'e2e_constraints_nodelex/copy-coverage/e2e-constraints-nodelex-copy-coverage_predictions.txt'
    postprocess(pred_file, ref_file, mrs_file, path_data, option='constraints-nodelex-copy-coverage')

    # relexicalise, delex
    path_to_model = path_models + 'e2e_constraints_delex/'
    preds_file_initial = 'e2e-constraints-delex_predictions.txt'
    relexicalise(path_to_model, preds_file_initial, constraints=True)

    pred_file = path_models + 'e2e_constraints_delex/relex_predictions.txt'
    postprocess(pred_file, ref_file, mrs_file, path_data, option='constraints-delex')

    # relexicalise, delex, copy and coverage
    path_to_model = path_models + 'e2e_constraints_delex/copy-coverage/'
    preds_file_initial = 'e2e-constraints-delex-copy-coverage_predictions.txt'
    relexicalise(path_to_model, preds_file_initial, constraints=True)

    pred_file = path_models + 'e2e_constraints_delex/copy-coverage/relex_predictions.txt'
    postprocess(pred_file, ref_file, mrs_file, path_data, option='constraints-delex-copy-coverage')


# gen_constraints_refs_files()
