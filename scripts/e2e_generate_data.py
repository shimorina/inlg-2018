from e2e_data_reading import E2EDataset


path = '/home/anastasia/Loria/thesis/e2e-dataset/'
file_train = 'trainset.csv'
file_dev = 'devset.csv'
file_test = 'testset_w_refs.csv'

# train
e2e = E2EDataset()
e2e.fill_dataset(path + file_train)
e2e.write_data('train-notdelex')
e2e.write_data('train-delex', delex=True)

# dev
e2e = E2EDataset()
e2e.fill_dataset(path + file_dev)
e2e.write_data('dev-notdelex')
e2e.write_data('dev-delex', delex=True)


# test
e2e = E2EDataset()
e2e.fill_dataset(path + file_test)
e2e.write_data('test-notdelex')
e2e.write_data('test-delex', delex=True)


'''print('Entries: ', e2e.entry_count())
print('Lexs: ', e2e.lexs_count())
patterns, patt_dict = e2e.mr_patterns()
print('MR patterns: ', len(patt_dict))


path_to_store_data = './e2e_files/'
with open(path_to_store_data + 'MRs_e2e_stats.txt', 'w+') as f:
    for k in sorted(patt_dict, key=patt_dict.get, reverse=True):
        f.write(k + '\t' + str(patt_dict[k]) + '\n')'''


def mrs_length_stats():
    e2e = E2EDataset()
    e2e.fill_dataset(path+file_train)
    e2e.fill_dataset(path+file_dev)
    e2e.fill_dataset(path+file_test)
    patterns, patt_dict = e2e.mr_patterns()
    slots_three_values = 0
    other_slots = 0
    for patt, count in patt_dict.items():
        if len(patt.split()) == 3:  # three slots
            slots_three_values += patt_dict[patt]
        else:
            other_slots += patt_dict[patt]
    print("# MRs with three slots", slots_three_values)  # 152
    print("# MRs with > 3 slots", other_slots)              # 5887


# mrs_length_stats()
