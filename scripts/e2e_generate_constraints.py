from e2e_data_reading import E2EDataset


path = '/home/anastasia/Loria/thesis/e2e-dataset/'
file_train = 'trainset.csv'
file_dev = 'devset.csv'
file_test = 'testset_w_refs.csv'


def entities_attr():
    """
    Output stats about values for each slot.
    :return:
    """
    e2e = initialise_e2e()
    attributes = ['area', 'customer rating', 'eatType', 'familyFriendly', 'food', 'name', 'near', 'priceRange']
    with open('e2e_files/entities_e2e.txt', 'w+') as f:
        for attribute in attributes:
            entities = e2e.entity_count(attribute)
            f.write('Entities for ' + attribute + ': ' + str(len(entities)) + '\n')
            for entity in entities:
                f.write(entity + '\n')
            f.write('\n')


# entities_attr()


def initialise_e2e():
    e2e = E2EDataset()
    e2e.fill_dataset(path+file_train)
    e2e.fill_dataset(path+file_dev)
    e2e.fill_dataset(path+file_test)
    # print('Entries: ', e2e.entry_count())
    # print('Lexs: ', e2e.lexs_count())
    return e2e


def generate_constraints(write_to_file=True):
    """
    Given some restaurants, calculate how many instances with those restaurants are in the whole corpus (train+dev+test)
    Put instances with 4 restaurants in test.
    Generate files for train/dev/test.
    :return:
    """
    e2e = initialise_e2e()
    e2e_test = e2e.copy()
    e2e_copy = e2e.copy()
    for entry in e2e_copy.entries:
        values = [val for attr, val in entry.mr.attrs]
        if 'Cocum' in values or 'Clowns' in values or 'Cotto' in values or 'Wildwood' in values:
            e2e.remove(entry)
        else:
            e2e_test.remove(entry)

    # divide into train and dev; put 547 entries in dev, as it was in the original split
    e2e_train = E2EDataset()
    e2e_dev = E2EDataset()
    e2e_train.add_entries(e2e.entries[:-547])
    e2e_dev.add_entries(e2e.entries[-547:])
    print('train entries', e2e_train.entry_count())
    print('dev entries', e2e_dev.entry_count())
    print('test entries', e2e_test.entry_count())
    print(e2e_train.slot_count())
    print(e2e_dev.slot_count())
    print(e2e_test.slot_count())

    if write_to_file:
        # write data for OpenNMT training
        e2e_train.write_data('train-constraints-notdelex')
        e2e_train.write_data('train-constraints-delex', delex=True)
        e2e_dev.write_data('dev-constraints-notdelex')
        e2e_dev.write_data('dev-constraints-delex', delex=True)
        e2e_test.write_data('test-constraints-notdelex')
        e2e_test.write_data('test-constraints-delex', delex=True)

    return e2e_test


# generate_constraints()
# generate_constraints(False)
