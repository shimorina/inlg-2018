import csv
from collections import defaultdict
from random import seed
from random import shuffle


class MeaningRepresentation:

    def __init__(self):
        self.attrs = []

    def fill(self, mr):
        # name[Alimentum], area[city centre], familyFriendly[yes], near[Burger King]
        for attr_value in mr.split(", "):
            attr, value = attr_value.split('[')
            self.attrs.append((attr, value[:-1]))  # remove the closing bracket

    def flat_mr(self):
        flat_str = ''
        for pair in self.attrs:
            flat_str += pair[0] + ' ' + pair[1] + ' '
        return flat_str.strip()

    def original_mr(self):
        orig_mrs = []
        for pair in self.attrs:
            orig_mrs.append(pair[0] + '[' + pair[1] + ']')
        return ', '.join(orig_mrs)

    def pattern(self):
        # a sequence of attributes
        sequence = [pair[0] for pair in self.attrs]
        return ' '.join(sorted(sequence)), sequence


class Entry:

    def __init__(self, original_mr):
        self.mr = MeaningRepresentation()
        self.original_mr = original_mr
        self.lexs = []

    def fill_lex(self, lex):
        self.lexs.append(lex)

    def fill_mr(self, mr):
        self.mr.fill(mr)


class E2EDataset:

    def __init__(self):
        self.entries = []

    def fill_dataset(self, datafile):
        with open(datafile, 'r') as csvfile:
            csvreader = csv.reader(csvfile)
            next(csvreader)  # skip the header
            mrs = set()
            for row in csvreader:
                mr_row = row[0]
                lex_row = row[1]
                if '\n' in lex_row or '\n' in mr_row:
                    lex_row = lex_row.replace('\n', '')  # delete new lines
                    mr_row = mr_row.replace('\n', '')  # delete new lines
                # make sure that MR is unique
                attrs = mr_row.split(", ")
                attrs.sort()
                new_mr_row = ', '.join(attrs)
                if new_mr_row not in mrs:
                    entry = Entry(mr_row)
                    entry.fill_lex(lex_row)
                    entry.fill_mr(mr_row)
                    self.entries.append(entry)
                else:
                    for entry in self.entries:
                        original_mr = entry.original_mr
                        if original_mr == mr_row:
                            entry.fill_lex(lex_row)
                            break
                # make sure that MR is unique
                mrs.add(new_mr_row)

    def entry_count(self):
        return len(self.entries)

    def lexs_count(self):
        # count without duplicates!!
        lexs = [len(set(entry.lexs)) for entry in self.entries]
        return sum(lexs)

    def write_data(self, option, delex=False, writefiles=True):
        mrs = []
        lexs = []
        replacements = []  # keep all the replacements that we made to write them to file
        for entry in self.entries:
            flat_mr = entry.mr.flat_mr()
            # don't consider duplicates in lexicalisations
            # preserve the original order
            for lex in del_dupl_order(entry.lexs):
                if not delex:
                    mrs.append(flat_mr)
                    lexs.append(lex)
                else:
                    # delexicalise
                    delex_mr, delex_lex, replacements = delexicalise(entry.mr, lex, replacements)
                    mrs.append(delex_mr)
                    lexs.append(delex_lex)

        # shuffle in the same way
        seed(10)
        if delex:
            c = list(zip(mrs, lexs, replacements))
            shuffle(c)
            mrs, lexs, replacements = zip(*c)
        else:
            c = list(zip(mrs, lexs))
            shuffle(c)
            mrs, lexs = zip(*c)

        # print('MRs len', len(mrs))
        # print('Lex len', len(lexs))
        if writefiles:
            with open(path_to_store_data + option + '-e2e.mrs', 'w+') as f:
                f.write('\n'.join(mrs))
            with open(path_to_store_data + option + '-e2e.lex', 'w+') as f:
                f.write('\n'.join(lexs))
        return replacements

    def mr_patterns(self):
        """
        Give a list of unique MR patterns, e.g., 'customer rating eatType name near'.
        :return: a list of patterns; a dictionary of patterns
        """
        patterns = []
        for entry in self.entries:
            patterns.append(entry.mr.pattern()[0])
        patterns_dict = defaultdict(int)
        for patt in patterns:
            patterns_dict[patt] += 1
        return patterns, patterns_dict

    def entity_count(self, attribute):
        """
        The entity list given the attribute.
        :param attribute:
        :return:
        """
        entities = []
        for entry in self.entries:
            attrs_values = entry.mr.attrs
            for att_val in attrs_values:
                if att_val[0] == attribute:
                    entities.append(att_val[1])
        return set(entities)

    def copy(self):
        """
        Copy the dataset.
        :return: a copy list
        """
        b_copy = E2EDataset()
        b_copy.entries = list(self.entries)
        return b_copy

    def remove(self, entry):
        self.entries.remove(entry)

    def add_entries(self, entries):
        """
        Add entries to E2E object.
        :return:
        """
        for entry in entries:
            self.entries.append(entry)

    def mrs(self):
        """
        Give a list of all MRs.
        :return:
        """
        mrs = []
        for entry in self.entries:
            mrs += [entry.mr.original_mr()]
        return mrs

    def slot_count(self):
        """
        Frequency distribution of the slots in MRs.
        :return:
        """
        slots_freq = defaultdict(int)
        for entry in self.entries:
            slots = [slvl[0] for slvl in entry.mr.attrs]
            for slot in slots:
                slots_freq[slot] += 1
        return slots_freq


def delexicalise(mr, lex, replacements):
    """
    Replace name[Restaurant] by NAME, near[Restaurant] by NEAR in MR.
    Replace corresponding names in lex.
    :param mr: an instance of MeaningRepresentation class
    :param lex: string
    :param replacements: list of tuples (replacements) made for each instance
    :return: delexicalised strings: lex and mr
    """
    near_val = ''  # may not be present
    delex_mr = ''
    replacement = []  # [(Restaurant, NAME), (Restaurant, NEAR)]
    for attr_val in mr.attrs:
        attr = attr_val[0]
        val = attr_val[1]
        if attr == 'name':
            name_val = val
            delex_mr += 'NAME' + ' '
            replacement.append((name_val, 'NAME'))
        elif attr == 'near':
            near_val = val
            delex_mr += 'NEAR' + ' '
            replacement.append((near_val, 'NEAR'))
        else:
            delex_mr += attr + ' ' + val + ' '
    # the name attribute is present in all data; near -- in the half only
    delex_lex = lex.replace(name_val, 'NAME')
    if near_val:
        delex_lex = delex_lex.replace(near_val, 'NEAR')
    replacements.append(replacement)
    return delex_mr, delex_lex, replacements


# path_to_store_data = './e2e_files/constraints/'
path_to_store_data = './e2e_files/original_split/'


def del_dupl_order(l):
    l_nodupl = []
    l_nodupl = [item for item in l if item not in l_nodupl]
    return l_nodupl
