import re
import os
import sys
from benchmark_reader import Benchmark
from benchmark_reader import select_files
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


# eight configs
configs = ['webnlg_delex',
           'webnlg_delex-cc',
           'webnlg_nodelex',
           'webnlg_nodelex-cc',
           'constraints_delex',
           'constraints_delex_cc',
           'constraints_nodelex',
           'constraints_nodelex_cc']


def calculate_ser():
    path = 'webnlg_ser/'
    # get all cleaned s and o from the whole corpus
    all_subj_cleaned, all_obj_cleaned = get_all_subj_obj()
    entities = list(set(all_subj_cleaned + all_obj_cleaned))
    # delete all numbers from entities
    for i, entity in enumerate(entities):
        try:
            float(entity)
            del entities[i]
        except ValueError:
            pass

    for config in configs:
        name = 'all-notdelex-orig-source.triple'
        if 'webnlg' in config:
            with open(path + 'webnlg_' + name, 'r') as f_mrs:
                mrs = f_mrs.readlines()
        else:
            with open(path + 'constraints_' + name, 'r') as f_mrs:
                mrs = f_mrs.readlines()
        with open(path + 'relex_pred_' + config + '.txt', 'r') as f_preds:
            preds = f_preds.readlines()
        se_rates = []
        for mr, pred in zip(mrs, preds):
            values = clean_mr(mr)
            total_n_slots = len(values)
            missing = 0
            hallucinated = 0
            for value in values:
                if value not in pred.lower():
                    missing += 1
            # delete s and o that are present in MR
            all_subj_obj_not_pres = [item for item in entities if item not in values]
            for entity in all_subj_obj_not_pres:
                if entity in pred.split():
                    hallucinated += 1
                    # print(entity)
                    # print(pred)
            ser = (missing + hallucinated) / total_n_slots
            se_rates.append(ser)
        av_ser_in_percentage = (sum(se_rates) / len(se_rates)) * 100
        print(config, round(av_ser_in_percentage, 2), '%')


def clean_mr(mr):
    # (19255)_1994_VK8 | density | 2.0(gramPerCubicCentimetres) | | |
    # extract all subjects and objects and clean them
    subj_obj = []
    triples = mr.strip().split('|||')  # the last one is empty
    triples = [triple for triple in triples if triple]  # delete empty triples
    for triple in triples:
        s, p, o = triple.split(' | ')
        s = s.lower().replace('_', ' ')
        o = o.lower().replace('_', ' ')
        # separate punct signs from text
        s = ' '.join(re.split('(\W)', s))
        o = ' '.join(re.split('(\W)', o))
        # delete white spaces
        subj_obj.append(' '.join(s.split()))
        subj_obj.append(' '.join(o.split()))

    return subj_obj


def get_all_subj_obj():
    # read all the webnlg corpus
    # extract all subjects and objects
    path_train = '/home/anastasia/Loria/webnlgchallenge/data2text/data/crowdflower/benchmark_release_v2/train'
    path_dev = '/home/anastasia/Loria/webnlgchallenge/data2text/data/crowdflower/benchmark_release_v2/dev'
    path_test = '/home/anastasia/Loria/webnlgchallenge/data2text/data/crowdflower/benchmark_release_v2/test'
    b = Benchmark()
    files_train = select_files(path_train)
    files_dev = select_files(path_dev)
    files_test = select_files(path_test)
    b.fill_benchmark(files_train + files_dev + files_test)
    subjs, objs = b.subjects_objects()
    # clean subj and obj
    subjs_cleaned = []
    for subj in list(subjs):
        subjs_cleaned.append(clean(subj))
    objs_cleaned = []
    for obj in list(objs):
        objs_cleaned.append(clean(obj))
    return subjs_cleaned, objs_cleaned


def clean(entity):
    entity = entity.lower().replace('_', ' ')
    # separate punct signs from text
    entity = ' '.join(re.split('(\W)', entity))
    entity = ' '.join(entity.split())  # delete whitespaces
    return entity


# calculate_ser()
