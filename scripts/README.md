# Requirements

Python3

Download [E2E](https://github.com/tuetschek/e2e-dataset/) and [WebNLG v2](https://gitlab.com/shimorina/webnlg-dataset) datasets.

# E2E

* `e2e_data_reading.py`

	General classes for the E2E dataset.

* `e2e_generate_data.py`

	Produce original train/dev/test data for the input to OpenNMT-py. Remove repetitions in (data, text) pairs.

* `e2e_generate_constraints.py`

	Generate constrained train/dev/test data for the input to OpenNMT-py.

* `e2e_relexicalise.py`

	Relexicalise delexicalised files. Produce files for the e2e-metrics output.

* `e2e_slot_error_rate.py`

	Compute SER.

# WebNLG

We used [scripts](https://gitlab.com/shimorina/webnlg-baseline) developed for the WebNLG Challenge 2017 [baseline](https://webnlg-challenge.loria.fr/challenge_2017/#webnlg-baseline-system).

* `webnlg_slot_error_rate.py`

	Compute SER.

