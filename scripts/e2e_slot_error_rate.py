attrs = ['area', 'customerRating', 'eatType', 'familyFriendly', 'food', 'name', 'near', 'priceRange']


def extract_slot_value_pairs(mr):
    """
    Extract slot-value pairs from flat representations.
    :param mr: flat representation: name Blue Spice eatType pub customer rating average near Crowne Plaza Hotel
    :return:
    """
    mr = mr.replace('customer rating', 'customerRating')
    pairs = {}
    for attr in attrs:
        if attr in mr:
            _, after = mr.split(attr, 1)  # take everything what is after the splitting word
            tokens = after.split(' ')
            value = ''
            # collect a value by searching for the next attribute
            for token in tokens:
                if token not in attrs:
                    value += token + ' '
                else:
                    break
            if not value:
                "Something is wrong"
            pairs[attr] = value.lower().strip()
    # delete binary slots
    if 'familyFriendly' in pairs.keys():
        del pairs['familyFriendly']

    return pairs


# eight configs
configs = ['delex',
           'delex-copy-coverage',
           'nodelex',
           'nodelex-copy-coverage',
           'constraints-delex',
           'constraints-delex-copy-coverage',
           'constraints-nodelex',
           'constraints-nodelex-copy-coverage']


def calculate_ser():
    path = 'e2e_files/slot_error_rate/'

    for config in configs:
        with open(path + 'mrs_' + config + '.txt', 'r') as f_mrs:
            mrs = f_mrs.readlines()
        with open(path + 'predictions_' + config + '.txt', 'r') as f_preds:
            preds = f_preds.readlines()
        se_rates = []
        for mr, pred in zip(mrs, preds):
            # restore slot-value pairs from test files
            pairs = extract_slot_value_pairs(mr.strip())
            total_n_slots = len(pairs)
            present = 0
            missing = 0
            hallucinated = 0
            for value in sorted(pairs.values()):
                if value in pred.lower():
                    present += 1
                else:
                    missing += 1
                    # print(mr)
                    # print(pred)
            # an approximation for hallucinated slots
            # take attributes that aren't in MR and check their presence in predictions
            not_present_attrs = [attr for attr in attrs if attr not in list(pairs.keys())]
            if 'customerRating' in not_present_attrs:
                not_present_attrs.append('customer rating')
            if 'priceRange' in not_present_attrs:
                not_present_attrs.append('price range')
            for item in not_present_attrs:
                if item in pred.split():
                    hallucinated += 1
                    # print(mr)
                    # print(pred)

            ser = (missing + hallucinated) / total_n_slots
            se_rates.append(ser)
        av_ser_in_percentage = (sum(se_rates) / len(se_rates)) * 100
        print(config, round(av_ser_in_percentage, 2), '%')


# calculate_ser()
