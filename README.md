Scripts, data, results for the paper

**[_Handling Rare Items in Data-to-Text Generation_](http://aclweb.org/anthology/W18-6543). A. Shimorina, C. Gardent. INLG 2018.** [[poster](inlg2018_poster.pdf)]

```
@InProceedings{shimorina2018handling,
  author = 	"Shimorina, Anastasia and Gardent, Claire",
  title = 	"Handling Rare Items in Data-to-Text Generation",
  booktitle = 	"Proceedings of the 11th International Conference on Natural Language Generation",
  year = 	"2018",
  publisher = 	"Association for Computational Linguistics",
  pages = 	"360--370",
  location = 	"Tilburg University, The Netherlands",
  url = 	"http://aclweb.org/anthology/W18-6543"
}
```

## Repository Structure

* `scripts`

	Scripts used to read, preprocess, write data. See [README](./scripts/README.md)

* `input_data`

	Files for input to neural models.

* `run_opennmtpy`

	Shell scripts to run OpenNMT-py.

* `output_predictions`

	See [README](./output_predictions/README.md)

* `e2e_ser`

	Files to calculate the slot-error rate for E2E.

* `webnlg_ser`

	Files to calculate the slot-error rate for WebNLG.

* `evaluation`

	Shell scripts to run automatic metrics.
	
	Human evaluation annotations.

* `correlation`

	Data, code, results for correlation analysis between all the metrics used.

