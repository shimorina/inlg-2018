#!/bin/bash

export PATHFILES=/home/anastasia/Loria/opennmtpy_models

# No Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/webnlg_nodelex/train-webnlg-release-all-notdelex.triple \
-train_tgt ${PATHFILES}/webnlg_nodelex/train-webnlg-release-all-notdelex.lex \
-valid_src ${PATHFILES}/webnlg_nodelex/dev-webnlg-release-all-notdelex.triple \
-valid_tgt ${PATHFILES}/webnlg_nodelex/dev-webnlg-release-all-notdelex.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/webnlg_nodelex/webnlg-webnlg-release-all-nodelex

# train
python3 train.py \
-data ${PATHFILES}/webnlg_nodelex/webnlg-webnlg-release-all-nodelex \
-save_model ${PATHFILES}/webnlg_nodelex/webnlg-webnlg-release-all-nodelex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/webnlg_nodelex/webnlg-webnlg-release-all-nodelex_*_e13.pt \
-src ${PATHFILES}/webnlg_nodelex/test-webnlg-release-all-notdelex.triple \
-output ${PATHFILES}/webnlg_nodelex/webnlg-webnlg-release-all-nodelex_predictions.txt \
-gpu 0
