#!/bin/bash

export PATHFILES=/home/anastasia/Loria/opennmtpy_models

# With Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/webnlg_delex/train-webnlg-release-all-delex.triple \
-train_tgt ${PATHFILES}/webnlg_delex/train-webnlg-release-all-delex.lex \
-valid_src ${PATHFILES}/webnlg_delex/dev-webnlg-release-all-delex.triple \
-valid_tgt ${PATHFILES}/webnlg_delex/dev-webnlg-release-all-delex.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/webnlg_delex/webnlg-webnlg-release-all-delex

# train
python3 train.py \
-data ${PATHFILES}/webnlg_delex/webnlg-webnlg-release-all-delex \
-save_model ${PATHFILES}/webnlg_delex/webnlg-webnlg-release-all-delex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/webnlg_delex/webnlg-webnlg-release-all-delex_*_e13.pt \
-src ${PATHFILES}/webnlg_delex/test-webnlg-release-all-delex.triple \
-output ${PATHFILES}/webnlg_delex/webnlg-webnlg-release-all-delex_predictions.txt \
-gpu 0
