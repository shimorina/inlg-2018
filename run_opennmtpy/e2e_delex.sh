#!/bin/bash

export PATHFILES=/home/anastasia/Loria/opennmtpy_models

# With Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/e2e_delex/train-delex-e2e.mrs \
-train_tgt ${PATHFILES}/e2e_delex/train-delex-e2e.lex \
-valid_src ${PATHFILES}/e2e_delex/dev-delex-e2e.mrs \
-valid_tgt ${PATHFILES}/e2e_delex/dev-delex-e2e.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/e2e_delex/e2e-delex

# train
python3 train.py \
-data ${PATHFILES}/e2e_delex/e2e-delex \
-save_model ${PATHFILES}/e2e_delex/e2e-delex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_delex/e2e-delex_*_e13.pt \
-src ${PATHFILES}/e2e_delex/test-delex-e2e.mrs \
-output ${PATHFILES}/e2e_delex/e2e-delex_predictions.txt \
-gpu 0
