#!/bin/bash

export PATHFILES=/home/anastasia/Loria/opennmtpy_models


######### NO CONSTRAINTS, ORIGINAL SPLIT ##############

# No Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/e2e_nodelex/train-notdelex-e2e.mrs \
-train_tgt ${PATHFILES}/e2e_nodelex/train-notdelex-e2e.lex \
-valid_src ${PATHFILES}/e2e_nodelex/dev-notdelex-e2e.mrs \
-valid_tgt ${PATHFILES}/e2e_nodelex/dev-notdelex-e2e.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/e2e_nodelex/e2e-nodelex

# train
python3 train.py \
-data ${PATHFILES}/e2e_nodelex/e2e-nodelex \
-save_model ${PATHFILES}/e2e_nodelex/e2e-nodelex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_nodelex/e2e-nodelex_*_e13.pt \
-src ${PATHFILES}/e2e_nodelex/test-notdelex-e2e.mrs \
-output ${PATHFILES}/e2e_nodelex/e2e-nodelex_predictions.txt \
-gpu 0
