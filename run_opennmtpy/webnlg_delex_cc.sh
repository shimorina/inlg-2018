#!/bin/bash

export PATHFILES=/home/anastasia/Loria/opennmtpy_models

# Delex, copy and coverage
# preprocess, need to use dynamic_dict
python3 preprocess.py \
-train_src ${PATHFILES}/webnlg_delex/train-webnlg-release-all-delex.triple \
-train_tgt ${PATHFILES}/webnlg_delex/train-webnlg-release-all-delex.lex \
-valid_src ${PATHFILES}/webnlg_delex/dev-webnlg-release-all-delex.triple \
-valid_tgt ${PATHFILES}/webnlg_delex/dev-webnlg-release-all-delex.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-dynamic_dict \
-save_data ${PATHFILES}/webnlg_delex/copy-coverage/webnlg-webnlg-release-all-delex-copy-coverage
# -share_vocab

# train
python3 train.py \
-data ${PATHFILES}/webnlg_delex/copy-coverage/webnlg-webnlg-release-all-delex-copy-coverage \
-save_model ${PATHFILES}/webnlg_delex/copy-coverage/webnlg-webnlg-release-all-delex-copy-coverage \
-gpuid 0 \
-copy_attn \
-coverage_attn

# translate
python3 translate.py \
-model ${PATHFILES}/webnlg_delex/copy-coverage/webnlg-webnlg-release-all-delex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/webnlg_delex/test-webnlg-release-all-delex.triple \
-output ${PATHFILES}/webnlg_delex/copy-coverage/webnlg-webnlg-release-all-delex-copy-coverage_predictions.txt \
-gpu 0
