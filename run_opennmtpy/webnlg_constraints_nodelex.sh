#!/bin/bash

export PATHFILES=/home/anastasia/Loria/opennmtpy_models

# No Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/webnlg_constraints_nodelex/train-release-constrain-all-notdelex.triple \
-train_tgt ${PATHFILES}/webnlg_constraints_nodelex/train-release-constrain-all-notdelex.lex \
-valid_src ${PATHFILES}/webnlg_constraints_nodelex/dev-release-constrain-all-notdelex.triple \
-valid_tgt ${PATHFILES}/webnlg_constraints_nodelex/dev-release-constrain-all-notdelex.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/webnlg_constraints_nodelex/webnlg-release-constrain-all-nodelex

# train
python3 train.py \
-data ${PATHFILES}/webnlg_constraints_nodelex/webnlg-release-constrain-all-nodelex \
-save_model ${PATHFILES}/webnlg_constraints_nodelex/webnlg-release-constrain-all-nodelex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/webnlg_constraints_nodelex/webnlg-release-constrain-all-nodelex_*_e13.pt \
-src ${PATHFILES}/webnlg_constraints_nodelex/test-release-constrain-all-notdelex.triple \
-output ${PATHFILES}/webnlg_constraints_nodelex/webnlg-release-constrain-all-nodelex_predictions.txt \
-gpu 0
