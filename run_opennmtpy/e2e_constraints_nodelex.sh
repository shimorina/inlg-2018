#!/bin/bash

export PATHFILES=/home/anastasia/Loria/opennmtpy_models

# No Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/e2e_constraints_nodelex/train-constraints-notdelex-e2e.mrs \
-train_tgt ${PATHFILES}/e2e_constraints_nodelex/train-constraints-notdelex-e2e.lex \
-valid_src ${PATHFILES}/e2e_constraints_nodelex/dev-constraints-notdelex-e2e.mrs \
-valid_tgt ${PATHFILES}/e2e_constraints_nodelex/dev-constraints-notdelex-e2e.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/e2e_constraints_nodelex/e2e-constraints-nodelex

# train
python3 train.py \
-data ${PATHFILES}/e2e_constraints_nodelex/e2e-constraints-nodelex \
-save_model ${PATHFILES}/e2e_constraints_nodelex/e2e-constraints-nodelex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_constraints_nodelex/e2e-constraints-nodelex_*_e13.pt \
-src ${PATHFILES}/e2e_constraints_nodelex/test-constraints-notdelex-e2e.mrs \
-output ${PATHFILES}/e2e_constraints_nodelex/e2e-constraints-nodelex_predictions.txt \
-gpu 0
