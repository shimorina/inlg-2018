#!/bin/bash

export PATHFILES=/home/anastasia/Loria/opennmtpy_models

#########CONSTRAINTS##############

# No Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/e2e_constraints_nodelex/train-constraints-notdelex-e2e.mrs \
-train_tgt ${PATHFILES}/e2e_constraints_nodelex/train-constraints-notdelex-e2e.lex \
-valid_src ${PATHFILES}/e2e_constraints_nodelex/dev-constraints-notdelex-e2e.mrs \
-valid_tgt ${PATHFILES}/e2e_constraints_nodelex/dev-constraints-notdelex-e2e.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/e2e_constraints_nodelex/e2e-constraints-nodelex

# train
python3 train.py \
-data ${PATHFILES}/e2e_constraints_nodelex/e2e-constraints-nodelex \
-save_model ${PATHFILES}/e2e_constraints_nodelex/e2e-constraints-nodelex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_constraints_nodelex/e2e-constraints-nodelex_*_e13.pt \
-src ${PATHFILES}/e2e_constraints_nodelex/test-constraints-notdelex-e2e.mrs \
-output ${PATHFILES}/e2e_constraints_nodelex/e2e-constraints-nodelex_predictions.txt \
-gpu 0

# No delex, copy and coverage
# preprocess, need to use dynamic_dict
python3 preprocess.py \
-train_src ${PATHFILES}/e2e_constraints_nodelex/train-constraints-notdelex-e2e.mrs \
-train_tgt ${PATHFILES}/e2e_constraints_nodelex/train-constraints-notdelex-e2e.lex \
-valid_src ${PATHFILES}/e2e_constraints_nodelex/dev-constraints-notdelex-e2e.mrs \
-valid_tgt ${PATHFILES}/e2e_constraints_nodelex/dev-constraints-notdelex-e2e.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-dynamic_dict \
-save_data ${PATHFILES}/e2e_constraints_nodelex/copy-coverage

# train
python3 train.py \
-data ${PATHFILES}/e2e_constraints_nodelex/copy-coverage \
-save_model ${PATHFILES}/e2e_constraints_nodelex/copy-coverage/e2e-constraints-nodelex-copy-coverage \
-gpuid 0 \
-copy_attn \
-coverage_attn

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_constraints_nodelex/copy-coverage/e2e-constraints-nodelex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/e2e_constraints_nodelex/test-constraints-notdelex-e2e.mrs \
-output ${PATHFILES}/e2e_constraints_nodelex/copy-coverage/e2e-constraints-nodelex-copy-coverage_predictions.txt \
-gpu 0

# With Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/e2e_constraints_delex/train-constraints-delex-e2e.mrs \
-train_tgt ${PATHFILES}/e2e_constraints_delex/train-constraints-delex-e2e.lex \
-valid_src ${PATHFILES}/e2e_constraints_delex/dev-constraints-delex-e2e.mrs \
-valid_tgt ${PATHFILES}/e2e_constraints_delex/dev-constraints-delex-e2e.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/e2e_constraints_delex/e2e-constraints-delex

# train
python3 train.py \
-data ${PATHFILES}/e2e_constraints_delex/e2e-constraints-delex \
-save_model ${PATHFILES}/e2e_constraints_delex/e2e-constraints-delex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_constraints_delex/e2e-constraints-delex_*_e13.pt \
-src ${PATHFILES}/e2e_constraints_delex/test-constraints-delex-e2e.mrs \
-output ${PATHFILES}/e2e_constraints_delex/e2e-constraints-delex_predictions.txt \
-gpu 0

# Delex, copy and coverage
# preprocess, need to use dynamic_dict
python3 preprocess.py \
-train_src ${PATHFILES}/e2e_constraints_delex/train-constraints-delex-e2e.mrs \
-train_tgt ${PATHFILES}/e2e_constraints_delex/train-constraints-delex-e2e.lex \
-valid_src ${PATHFILES}/e2e_constraints_delex/dev-constraints-delex-e2e.mrs \
-valid_tgt ${PATHFILES}/e2e_constraints_delex/dev-constraints-delex-e2e.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-dynamic_dict \
-save_data ${PATHFILES}/e2e_constraints_delex/copy-coverage/e2e-constraints-delex-copy-coverage
# -share_vocab

# train
python3 train.py \
-data ${PATHFILES}/e2e_constraints_delex/copy-coverage/e2e-constraints-delex-copy-coverage \
-save_model ${PATHFILES}/e2e_constraints_delex/copy-coverage/e2e-constraints-delex-copy-coverage \
-gpuid 0 \
-copy_attn \
-coverage_attn

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_constraints_delex/copy-coverage/e2e-constraints-delex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/e2e_constraints_delex/test-constraints-delex-e2e.mrs \
-output ${PATHFILES}/e2e_constraints_delex/copy-coverage/e2e-constraints-delex-copy-coverage_predictions.txt \
-gpu 0


######### NO CONSTRAINTS, ORIGINAL SPLIT ##############

# No Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/e2e_nodelex/train-notdelex-e2e.mrs \
-train_tgt ${PATHFILES}/e2e_nodelex/train-notdelex-e2e.lex \
-valid_src ${PATHFILES}/e2e_nodelex/dev-notdelex-e2e.mrs \
-valid_tgt ${PATHFILES}/e2e_nodelex/dev-notdelex-e2e.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/e2e_nodelex/e2e-nodelex

# train
python3 train.py \
-data ${PATHFILES}/e2e_nodelex/e2e-nodelex \
-save_model ${PATHFILES}/e2e_nodelex/e2e-nodelex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_nodelex/e2e-nodelex_*_e13.pt \
-src ${PATHFILES}/e2e_nodelex/test-notdelex-e2e.mrs \
-output ${PATHFILES}/e2e_nodelex/e2e-nodelex_predictions.txt \
-gpu 0

# No delex, copy and coverage
# preprocess, need to use dynamic_dict
python3 preprocess.py \
-train_src ${PATHFILES}/e2e_nodelex/train-notdelex-e2e.mrs \
-train_tgt ${PATHFILES}/e2e_nodelex/train-notdelex-e2e.lex \
-valid_src ${PATHFILES}/e2e_nodelex/dev-notdelex-e2e.mrs \
-valid_tgt ${PATHFILES}/e2e_nodelex/dev-notdelex-e2e.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-dynamic_dict \
-save_data ${PATHFILES}/e2e_nodelex/copy-coverage/e2e-nodelex-copy-coverage
# -share_vocab

# train
python3 train.py \
-data ${PATHFILES}/e2e_nodelex/copy-coverage/e2e-nodelex-copy-coverage \
-save_model ${PATHFILES}/e2e_nodelex/copy-coverage/e2e-nodelex-copy-coverage \
-gpuid 0 \
-copy_attn \
-coverage_attn

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_nodelex/copy-coverage/e2e-nodelex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/e2e_nodelex/test-notdelex-e2e.mrs \
-output ${PATHFILES}/e2e_nodelex/copy-coverage/e2e-nodelex-copy-coverage_predictions.txt \
-gpu 0


# With Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/e2e_delex/train-delex-e2e.mrs \
-train_tgt ${PATHFILES}/e2e_delex/train-delex-e2e.lex \
-valid_src ${PATHFILES}/e2e_delex/dev-delex-e2e.mrs \
-valid_tgt ${PATHFILES}/e2e_delex/dev-delex-e2e.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/e2e_delex/e2e-delex

# train
python3 train.py \
-data ${PATHFILES}/e2e_delex/e2e-delex \
-save_model ${PATHFILES}/e2e_delex/e2e-delex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_delex/e2e-delex_*_e13.pt \
-src ${PATHFILES}/e2e_delex/test-delex-e2e.mrs \
-output ${PATHFILES}/e2e_delex/e2e-delex_predictions.txt \
-gpu 0

# Delex, copy and coverage
# preprocess, need to use dynamic_dict
python3 preprocess.py \
-train_src ${PATHFILES}/e2e_delex/train-delex-e2e.mrs \
-train_tgt ${PATHFILES}/e2e_delex/train-delex-e2e.lex \
-valid_src ${PATHFILES}/e2e_delex/dev-delex-e2e.mrs \
-valid_tgt ${PATHFILES}/e2e_delex/dev-delex-e2e.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-dynamic_dict \
-save_data ${PATHFILES}/e2e_delex/copy-coverage/e2e-delex-copy-coverage
# -share_vocab

# train
python3 train.py \
-data ${PATHFILES}/e2e_delex/copy-coverage/e2e-delex-copy-coverage \
-save_model ${PATHFILES}/e2e_delex/copy-coverage/e2e-delex-copy-coverage \
-gpuid 0 \
-copy_attn \
-coverage_attn

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_delex/copy-coverage/e2e-delex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/e2e_delex/test-delex-e2e.mrs \
-output ${PATHFILES}/e2e_delex/copy-coverage/e2e-delex-copy-coverage_predictions.txt \
-gpu 0


# ADVERSARIAL EXAMPLES

python3 translate.py \
-model ${PATHFILES}/e2e_delex/e2e-delex_*_e13.pt \
-src ${PATHFILES}/e2e_delex/adversarial.mrs \
-output ${PATHFILES}/e2e_delex/adversarial_predictions.txt \
-gpu 0

python3 translate.py \
-model ${PATHFILES}/e2e_delex/copy-coverage/e2e-delex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/e2e_delex/adversarial.mrs \
-output ${PATHFILES}/e2e_delex/copy-coverage/adversarial_predictions.txt \
-gpu 0

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_nodelex/e2e-nodelex_*_e13.pt \
-src ${PATHFILES}/e2e_nodelex/adversarial.mrs \
-output ${PATHFILES}/e2e_nodelex/adversarial_predictions.txt \
-gpu 0

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_nodelex/copy-coverage/e2e-nodelex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/e2e_nodelex/adversarial.mrs \
-output ${PATHFILES}/e2e_nodelex/copy-coverage/adversarial_predictions.txt \
-gpu 0


