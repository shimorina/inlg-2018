#!/bin/bash

export PATHFILES=/home/anastasia/Loria/opennmtpy_models

# No delex, copy and coverage
# preprocess, need to use dynamic_dict
python3 preprocess.py \
-train_src ${PATHFILES}/e2e_nodelex/train-notdelex-e2e.mrs \
-train_tgt ${PATHFILES}/e2e_nodelex/train-notdelex-e2e.lex \
-valid_src ${PATHFILES}/e2e_nodelex/dev-notdelex-e2e.mrs \
-valid_tgt ${PATHFILES}/e2e_nodelex/dev-notdelex-e2e.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-dynamic_dict \
-save_data ${PATHFILES}/e2e_nodelex/copy-coverage/e2e-nodelex-copy-coverage
# -share_vocab

# train
python3 train.py \
-data ${PATHFILES}/e2e_nodelex/copy-coverage/e2e-nodelex-copy-coverage \
-save_model ${PATHFILES}/e2e_nodelex/copy-coverage/e2e-nodelex-copy-coverage \
-gpuid 0 \
-copy_attn \
-coverage_attn

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_nodelex/copy-coverage/e2e-nodelex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/e2e_nodelex/test-notdelex-e2e.mrs \
-output ${PATHFILES}/e2e_nodelex/copy-coverage/e2e-nodelex-copy-coverage_predictions.txt \
-gpu 0
