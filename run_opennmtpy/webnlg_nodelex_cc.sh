#!/bin/bash

export PATHFILES=/home/anastasia/Loria/opennmtpy_models

# No delex, copy and coverage
# preprocess, need to use dynamic_dict
python3 preprocess.py \
-train_src ${PATHFILES}/webnlg_nodelex/train-webnlg-release-all-notdelex.triple \
-train_tgt ${PATHFILES}/webnlg_nodelex/train-webnlg-release-all-notdelex.lex \
-valid_src ${PATHFILES}/webnlg_nodelex/dev-webnlg-release-all-notdelex.triple \
-valid_tgt ${PATHFILES}/webnlg_nodelex/dev-webnlg-release-all-notdelex.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-dynamic_dict \
-save_data ${PATHFILES}/webnlg_nodelex/copy-coverage/webnlg-webnlg-release-all-nodelex-copy-coverage
# -share_vocab

# train
python3 train.py \
-data ${PATHFILES}/webnlg_nodelex/copy-coverage/webnlg-webnlg-release-all-nodelex-copy-coverage \
-save_model ${PATHFILES}/webnlg_nodelex/copy-coverage/webnlg-webnlg-release-all-nodelex-copy-coverage \
-gpuid 0 \
-copy_attn \
-coverage_attn

# translate
python3 translate.py \
-model ${PATHFILES}/webnlg_nodelex/copy-coverage/webnlg-webnlg-release-all-nodelex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/webnlg_nodelex/test-webnlg-release-all-notdelex.triple \
-output ${PATHFILES}/webnlg_nodelex/copy-coverage/webnlg-webnlg-release-all-nodelex-copy-coverage_predictions.txt \
-gpu 0

