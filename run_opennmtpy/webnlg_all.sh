#!/bin/bash

export PATHFILES=/home/anastasia/Loria/opennmtpy_models

#########CONSTRAINTS##############

# No Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/webnlg_constraints_nodelex/train-release-constrain-all-notdelex.triple \
-train_tgt ${PATHFILES}/webnlg_constraints_nodelex/train-release-constrain-all-notdelex.lex \
-valid_src ${PATHFILES}/webnlg_constraints_nodelex/dev-release-constrain-all-notdelex.triple \
-valid_tgt ${PATHFILES}/webnlg_constraints_nodelex/dev-release-constrain-all-notdelex.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/webnlg_constraints_nodelex/webnlg-release-constrain-all-nodelex

# train
python3 train.py \
-data ${PATHFILES}/webnlg_constraints_nodelex/webnlg-release-constrain-all-nodelex \
-save_model ${PATHFILES}/webnlg_constraints_nodelex/webnlg-release-constrain-all-nodelex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/webnlg_constraints_nodelex/webnlg-release-constrain-all-nodelex_*_e13.pt \
-src ${PATHFILES}/webnlg_constraints_nodelex/test-release-constrain-all-notdelex.triple \
-output ${PATHFILES}/webnlg_constraints_nodelex/webnlg-release-constrain-all-nodelex_predictions.txt \
-gpu 0

# No delex, copy and coverage
# preprocess, need to use dynamic_dict
python3 preprocess.py \
-train_src ${PATHFILES}/webnlg_constraints_nodelex/train-release-constrain-all-notdelex.triple \
-train_tgt ${PATHFILES}/webnlg_constraints_nodelex/train-release-constrain-all-notdelex.lex \
-valid_src ${PATHFILES}/webnlg_constraints_nodelex/dev-release-constrain-all-notdelex.triple \
-valid_tgt ${PATHFILES}/webnlg_constraints_nodelex/dev-release-constrain-all-notdelex.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-dynamic_dict \
-save_data ${PATHFILES}/webnlg_constraints_nodelex/copy-coverage/webnlg-release-constrain-all-nodelex-copy-coverage
# -share_vocab

# train
python3 train.py \
-data ${PATHFILES}/webnlg_constraints_nodelex/copy-coverage \
-save_model ${PATHFILES}/webnlg_constraints_nodelex/copy-coverage/webnlg-release-constrain-all-nodelex-copy-coverage \
-gpuid 0 \
-copy_attn \
-coverage_attn

# translate
python3 translate.py \
-model ${PATHFILES}/webnlg_constraints_nodelex/copy-coverage/webnlg-release-constrain-all-nodelex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/webnlg_constraints_nodelex/test-release-constrain-all-notdelex.triple \
-output ${PATHFILES}/webnlg_constraints_nodelex/copy-coverage/webnlg-release-constrain-all-nodelex-copy-coverage_predictions.txt \
-gpu 0

# With Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/webnlg_constraints_delex/train-release-constrain-all-delex.triple \
-train_tgt ${PATHFILES}/webnlg_constraints_delex/train-release-constrain-all-delex.lex \
-valid_src ${PATHFILES}/webnlg_constraints_delex/dev-release-constrain-all-delex.triple \
-valid_tgt ${PATHFILES}/webnlg_constraints_delex/dev-release-constrain-all-delex.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/webnlg_constraints_delex/webnlg-release-constrain-all-delex

# train
python3 train.py \
-data ${PATHFILES}/webnlg_constraints_delex/webnlg-release-constrain-all-delex \
-save_model ${PATHFILES}/webnlg_constraints_delex/webnlg-release-constrain-all-delex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/webnlg_constraints_delex/webnlg-release-constrain-all-delex_*_e13.pt \
-src ${PATHFILES}/webnlg_constraints_delex/test-release-constrain-all-delex.triple \
-output ${PATHFILES}/webnlg_constraints_delex/webnlg-release-constrain-all-delex_predictions.txt \
-gpu 0

# Delex, copy and coverage
# preprocess, need to use dynamic_dict
python3 preprocess.py \
-train_src ${PATHFILES}/webnlg_constraints_delex/train-release-constrain-all-delex.triple \
-train_tgt ${PATHFILES}/webnlg_constraints_delex/train-release-constrain-all-delex.lex \
-valid_src ${PATHFILES}/webnlg_constraints_delex/dev-release-constrain-all-delex.triple \
-valid_tgt ${PATHFILES}/webnlg_constraints_delex/dev-release-constrain-all-delex.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-dynamic_dict \
-save_data ${PATHFILES}/webnlg_constraints_delex/copy-coverage/webnlg-release-constrain-all-delex-copy-coverage
# -share_vocab

# train
python3 train.py \
-data ${PATHFILES}/webnlg_constraints_delex/copy-coverage/webnlg-release-constrain-all-delex-copy-coverage \
-save_model ${PATHFILES}/webnlg_constraints_delex/copy-coverage/webnlg-release-constrain-all-delex-copy-coverage \
-gpuid 0 \
-copy_attn \
-coverage_attn

# translate
python3 translate.py \
-model ${PATHFILES}/webnlg_constraints_delex/copy-coverage/webnlg-release-constrain-all-delex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/webnlg_constraints_delex/test-release-constrain-all-delex.triple \
-output ${PATHFILES}/webnlg_constraints_delex/copy-coverage/webnlg-release-constrain-all-delex-copy-coverage_predictions.txt \
-gpu 0


#########  NO CONSTRAINTS, ORIGINAL SPLIT##############

# No Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/webnlg_nodelex/train-webnlg-release-all-notdelex.triple \
-train_tgt ${PATHFILES}/webnlg_nodelex/train-webnlg-release-all-notdelex.lex \
-valid_src ${PATHFILES}/webnlg_nodelex/dev-webnlg-release-all-notdelex.triple \
-valid_tgt ${PATHFILES}/webnlg_nodelex/dev-webnlg-release-all-notdelex.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/webnlg_nodelex/webnlg-webnlg-release-all-nodelex

# train
python3 train.py \
-data ${PATHFILES}/webnlg_nodelex/webnlg-webnlg-release-all-nodelex \
-save_model ${PATHFILES}/webnlg_nodelex/webnlg-webnlg-release-all-nodelex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/webnlg_nodelex/webnlg-webnlg-release-all-nodelex_*_e13.pt \
-src ${PATHFILES}/webnlg_nodelex/test-webnlg-release-all-notdelex.triple \
-output ${PATHFILES}/webnlg_nodelex/webnlg-webnlg-release-all-nodelex_predictions.txt \
-gpu 0

# No delex, copy and coverage
# preprocess, need to use dynamic_dict
python3 preprocess.py \
-train_src ${PATHFILES}/webnlg_nodelex/train-webnlg-release-all-notdelex.triple \
-train_tgt ${PATHFILES}/webnlg_nodelex/train-webnlg-release-all-notdelex.lex \
-valid_src ${PATHFILES}/webnlg_nodelex/dev-webnlg-release-all-notdelex.triple \
-valid_tgt ${PATHFILES}/webnlg_nodelex/dev-webnlg-release-all-notdelex.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-dynamic_dict \
-save_data ${PATHFILES}/webnlg_nodelex/copy-coverage/webnlg-webnlg-release-all-nodelex-copy-coverage
# -share_vocab

# train
python3 train.py \
-data ${PATHFILES}/webnlg_nodelex/copy-coverage/webnlg-webnlg-release-all-nodelex-copy-coverage \
-save_model ${PATHFILES}/webnlg_nodelex/copy-coverage/webnlg-webnlg-release-all-nodelex-copy-coverage \
-gpuid 0 \
-copy_attn \
-coverage_attn

# translate
python3 translate.py \
-model ${PATHFILES}/webnlg_nodelex/copy-coverage/webnlg-webnlg-release-all-nodelex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/webnlg_nodelex/test-webnlg-release-all-notdelex.triple \
-output ${PATHFILES}/webnlg_nodelex/copy-coverage/webnlg-webnlg-release-all-nodelex-copy-coverage_predictions.txt \
-gpu 0

# With Delexicalisation
# preprocess
python3 preprocess.py \
-train_src ${PATHFILES}/webnlg_delex/train-webnlg-release-all-delex.triple \
-train_tgt ${PATHFILES}/webnlg_delex/train-webnlg-release-all-delex.lex \
-valid_src ${PATHFILES}/webnlg_delex/dev-webnlg-release-all-delex.triple \
-valid_tgt ${PATHFILES}/webnlg_delex/dev-webnlg-release-all-delex.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-save_data ${PATHFILES}/webnlg_delex/webnlg-webnlg-release-all-delex

# train
python3 train.py \
-data ${PATHFILES}/webnlg_delex/webnlg-webnlg-release-all-delex \
-save_model ${PATHFILES}/webnlg_delex/webnlg-webnlg-release-all-delex \
-gpuid 0

# translate
python3 translate.py \
-model ${PATHFILES}/webnlg_delex/webnlg-webnlg-release-all-delex_*_e13.pt \
-src ${PATHFILES}/webnlg_delex/test-webnlg-release-all-delex.triple \
-output ${PATHFILES}/webnlg_delex/webnlg-webnlg-release-all-delex_predictions.txt \
-gpu 0

# Delex, copy and coverage
# preprocess, need to use dynamic_dict
python3 preprocess.py \
-train_src ${PATHFILES}/webnlg_delex/train-webnlg-release-all-delex.triple \
-train_tgt ${PATHFILES}/webnlg_delex/train-webnlg-release-all-delex.lex \
-valid_src ${PATHFILES}/webnlg_delex/dev-webnlg-release-all-delex.triple \
-valid_tgt ${PATHFILES}/webnlg_delex/dev-webnlg-release-all-delex.lex \
-src_seq_length 70 \
-tgt_seq_length 70 \
-dynamic_dict \
-save_data ${PATHFILES}/webnlg_delex/copy-coverage/webnlg-webnlg-release-all-delex-copy-coverage
# -share_vocab

# train
python3 train.py \
-data ${PATHFILES}/webnlg_delex/copy-coverage/webnlg-webnlg-release-all-delex-copy-coverage \
-save_model ${PATHFILES}/webnlg_delex/copy-coverage/webnlg-webnlg-release-all-delex-copy-coverage \
-gpuid 0 \
-copy_attn \
-coverage_attn

# translate
python3 translate.py \
-model ${PATHFILES}/webnlg_delex/copy-coverage/webnlg-webnlg-release-all-delex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/webnlg_delex/test-webnlg-release-all-delex.triple \
-output ${PATHFILES}/webnlg_delex/copy-coverage/webnlg-webnlg-release-all-delex-copy-coverage_predictions.txt \
-gpu 0


