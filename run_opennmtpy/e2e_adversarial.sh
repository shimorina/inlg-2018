#!/bin/bash

export PATHFILES=/home/anastasia/Loria/opennmtpy_models

# ADVERSARIAL EXAMPLES

python3 translate.py \
-model ${PATHFILES}/e2e_delex/e2e-delex_*_e13.pt \
-src ${PATHFILES}/e2e_delex/adversarial.mrs \
-output ${PATHFILES}/e2e_delex/adversarial_predictions.txt \
-gpu 0

python3 translate.py \
-model ${PATHFILES}/e2e_delex/copy-coverage/e2e-delex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/e2e_delex/adversarial.mrs \
-output ${PATHFILES}/e2e_delex/copy-coverage/adversarial_predictions.txt \
-gpu 0

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_nodelex/e2e-nodelex_*_e13.pt \
-src ${PATHFILES}/e2e_nodelex/adversarial.mrs \
-output ${PATHFILES}/e2e_nodelex/adversarial_predictions.txt \
-gpu 0

# translate
python3 translate.py \
-model ${PATHFILES}/e2e_nodelex/copy-coverage/e2e-nodelex-copy-coverage_*_e13.pt \
-src ${PATHFILES}/e2e_nodelex/adversarial.mrs \
-output ${PATHFILES}/e2e_nodelex/copy-coverage/adversarial_predictions.txt \
-gpu 0
