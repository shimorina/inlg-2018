#!/bin/bash

export PATHFILES=/home/anastasia/Loria/thesis/code/e2e_files

# Use measure_scores.py from here: https://github.com/tuetschek/e2e-metrics

# constraints nodelex, opennmtpy
echo "constraints nodelex, opennmtpy"
./measure_scores.py ${PATHFILES}/constraints_opennmtpy/refs_constraints-nodelex.txt ${PATHFILES}/constraints_opennmtpy/predictions_constraints-nodelex.txt > ${PATHFILES}/constraints_opennmtpy/metrics_constraints-nodelex.txt

# constraints delex, opennmtpy
echo "constraints delex, opennmtpy"
./measure_scores.py ${PATHFILES}/constraints_opennmtpy/refs_constraints-delex.txt ${PATHFILES}/constraints_opennmtpy/predictions_constraints-delex.txt > ${PATHFILES}/constraints_opennmtpy/metrics_constraints-delex.txt

# nodelex, opennmtpy
echo "nodelex, opennmtpy"
./measure_scores.py ${PATHFILES}/original_split_opennmtpy/refs_nodelex.txt ${PATHFILES}/original_split_opennmtpy/predictions_nodelex.txt > ${PATHFILES}/original_split_opennmtpy/metrics_nodelex.txt

# delex, opennmtpy
echo "delex, opennmtpy"
./measure_scores.py ${PATHFILES}/original_split_opennmtpy/refs_delex.txt ${PATHFILES}/original_split_opennmtpy/predictions_delex.txt > ${PATHFILES}/original_split_opennmtpy/metrics_delex.txt


# constraints nodelex, copy and coverage, opennmtpy
echo "constraints nodelex, opennmtpy, copy and coverage"
./measure_scores.py ${PATHFILES}/constraints_opennmtpy/refs_constraints-nodelex-copy-coverage.txt ${PATHFILES}/constraints_opennmtpy/predictions_constraints-nodelex-copy-coverage.txt > ${PATHFILES}/constraints_opennmtpy/metrics_constraints-nodelex-copy-coverage.txt

# constraints delex, copy and coverage, opennmtpy
echo "constraints delex, opennmtpy, copy and coverage"
./measure_scores.py ${PATHFILES}/constraints_opennmtpy/refs_constraints-delex-copy-coverage.txt ${PATHFILES}/constraints_opennmtpy/predictions_constraints-delex-copy-coverage.txt > ${PATHFILES}/constraints_opennmtpy/metrics_constraints-delex-copy-coverage.txt

# nodelex, copy and coverage, opennmtpy
echo "nodelex, opennmtpy, copy and coverage"
./measure_scores.py ${PATHFILES}/original_split_opennmtpy/refs_nodelex-copy-coverage.txt ${PATHFILES}/original_split_opennmtpy/predictions_nodelex-copy-coverage.txt > ${PATHFILES}/original_split_opennmtpy/metrics_nodelex-copy-coverage.txt

# delex, copy and coverage, opennmtpy
echo "delex, opennmtpy, copy and coverage"
./measure_scores.py ${PATHFILES}/original_split_opennmtpy/refs_delex-copy-coverage.txt ${PATHFILES}/original_split_opennmtpy/predictions_delex-copy-coverage.txt > ${PATHFILES}/original_split_opennmtpy/metrics_delex-copy-coverage.txt

