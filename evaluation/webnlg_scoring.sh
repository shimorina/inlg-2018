#!/bin/bash

export PATHFILES=/home/anastasia/Loria/webnlgchallenge/challenge/code/baseline

# Use measure_scores.py from here: https://github.com/tuetschek/e2e-metrics

# constraints nodelex, opennmtpy
echo "constraints nodelex, opennmtpy"
./measure_scores.py ${PATHFILES}/files_release_constraints/opennmt-py_output/refs-e2e-format.txt ${PATHFILES}/files_release_constraints/opennmt-py_output/relex_pred_constraints_nodelex.txt > ${PATHFILES}/files_release_constraints/opennmt-py_output/metrics_release-constraints-nodelex.txt

# constraints delex, opennmtpy
echo "constraints delex, opennmtpy"
./measure_scores.py ${PATHFILES}/files_release_constraints/opennmt-py_output/refs-e2e-format.txt ${PATHFILES}/files_release_constraints/opennmt-py_output/relex_pred_constraints_delex.txt > ${PATHFILES}/files_release_constraints/opennmt-py_output/metrics_release-constraints-delex.txt

# nodelex, opennmtpy
echo "nodelex, opennmtpy"
./measure_scores.py ${PATHFILES}/files_release_temp_with_dev/refs-e2e-format.txt ${PATHFILES}/files_release_temp_with_dev/relex_pred_webnlg_nodelex.txt > ${PATHFILES}/files_release_temp_with_dev/metrics_nodelex.txt

# delex, opennmtpy
echo "delex, opennmtpy"
./measure_scores.py ${PATHFILES}/files_release_temp_with_dev/refs-e2e-format.txt ${PATHFILES}/files_release_temp_with_dev/relex_pred_webnlg_delex.txt > ${PATHFILES}/files_release_temp_with_dev/metrics_delex.txt



# constraints nodelex, copy and coverage, opennmtpy
echo "constraints nodelex, opennmtpy, copy and coverage"
./measure_scores.py ${PATHFILES}/files_release_constraints/opennmt-py_output/refs-e2e-format.txt ${PATHFILES}/files_release_constraints/opennmt-py_output/relex_pred_constraints_nodelex_cc.txt > ${PATHFILES}/files_release_constraints/opennmt-py_output/metrics_release-constraints-nodelex-copy-coverage.txt

# constraints delex, copy and coverage, opennmtpy
echo "constraints delex, opennmtpy, copy and coverage"
./measure_scores.py ${PATHFILES}/files_release_constraints/opennmt-py_output/refs-e2e-format.txt ${PATHFILES}/files_release_constraints/opennmt-py_output/relex_pred_constraints_delex_cc.txt > ${PATHFILES}/files_release_constraints/opennmt-py_output/metrics_release-constraints-delex-copy-coverage.txt

# nodelex, copy and coverage, opennmtpy
echo "nodelex, opennmtpy, copy and coverage"
./measure_scores.py ${PATHFILES}/files_release_temp_with_dev/refs-e2e-format.txt ${PATHFILES}/files_release_temp_with_dev/relex_pred_webnlg_nodelex-cc.txt > ${PATHFILES}/files_release_temp_with_dev/metrics_nodelex-copy-coverage.txt

# delex, copy and coverage, opennmtpy
echo "delex, opennmtpy, copy and coverage"
./measure_scores.py ${PATHFILES}/files_release_temp_with_dev/refs-e2e-format.txt ${PATHFILES}/files_release_temp_with_dev/relex_pred_webnlg_delex-cc.txt > ${PATHFILES}/files_release_temp_with_dev/metrics_delex-copy-coverage.txt


